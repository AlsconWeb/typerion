<?php
/**
 * Area map widget.
 *
 * @package typerion/theme
 */

namespace Tryperion\Theme\Elementor;

use Elementor\Controls_Manager;
use Elementor\Core\Admin\Menu\Main;
use Elementor\Widget_Base;

/**
 * Map class file.
 */
class Map extends Widget_Base {

	/**
	 * Class constructor.
	 *
	 * @param array $data Widget data.
	 * @param array $args Widget arguments.
	 */
	public function __construct( $data = [], $args = null ) {
		parent::__construct( $data, $args );

		wp_register_style( 'map', get_stylesheet_directory_uri() . '/assets/css/main.css', '', \Tryperion\Theme\Main::TR_THEME_VERSION );
		wp_register_script( 'map', get_stylesheet_directory_uri() . '/assets/js/main.js', [ 'jquery' ], \Tryperion\Theme\Main::TR_THEME_VERSION, true );
	}

	/**
	 * Get name.
	 *
	 * @return string
	 */
	public function get_name() {
		return 'area_map';
	}

	/**
	 * Get widget title.
	 *
	 * @return string|null
	 */
	public function get_title() {
		return __( 'Area Map', 'typerion' );
	}

	/**
	 * Get icon
	 *
	 * @return string
	 */
	public function get_icon(): string {
		return 'eicon-map-pin';
	}

	/**
	 * Get category.
	 *
	 * @return string[]
	 */
	public function get_categories(): array {
		return [ 'general' ];
	}

	/**
	 * Add fields in element.
	 *
	 * @return void
	 */
	protected function _register_controls(): void {
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'elementor-awesomesauce' ),
			]
		);

		$this->end_controls_section();
	}


	/**
	 * Render in front-end.
	 *
	 * @return void
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();
		$this->add_inline_editing_attributes( 'title', 'none' );
		$this->add_inline_editing_attributes( 'description', 'basic' );
		$this->add_inline_editing_attributes( 'content', 'advanced' );

		include 'Templates/Map/template.php';
	}

}
