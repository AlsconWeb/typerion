<?php
/**
 * Template map.
 *
 * @package typerion/theme
 */


use Tryperion\Theme\CPT;

$filter_terms = get_terms(
	[
		'taxonomy'   => CPT::TR_CT_AREA_CATEGORY,
		'hide_empty' => true,
	]
);

$taxonomy_filter = ! empty( $_GET['filter'] ) ? filter_var( wp_unslash( $_GET['filter'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

$arg = [
	'post_type'      => CPT::TR_CPT_AREA_NAME,
	'posts_per_page' => - 1,
	'meta_query'     => [
		'relation' => 'AND',
		[
			'key'          => '_tr_area_lat',
			'meta_compare' => 'EXISTS',
		],
		[
			'key'          => '_tr_area_lng',
			'meta_compare' => 'EXISTS',
		],
	],
];

if ( $taxonomy_filter ) {
	$arg['tax_query'] = [
		[
			'taxonomy' => CPT::TR_CT_AREA_CATEGORY,
			'field'    => 'slug',
			'terms'    => $taxonomy_filter,
		],
	];
}

$properties_query = new WP_Query( $arg );
?>
<div class="filter-block">
	<ul class="nav-tabs">
		<li class="active">
			<a class="icon-map" href="#map-block" data-toggle="tab">
				<?php esc_html_e( 'Map', 'typerion' ); ?>
			</a>
		</li>
		<li>
			<a class="icon-grid" href="#grid" data-toggle="tab">
				<?php esc_html_e( 'Grid', 'typerion' ); ?>
			</a>
		</li>
	</ul>
	<div class="filter">
		<?php if ( ! empty( $filter_terms ) && ! is_wp_error( $filter_terms ) ) { ?>
			<p><?php esc_html_e( 'Select Property Type:', 'typerion' ); ?></p>
			<ul class="filter-buttons">
				<?php foreach ( $filter_terms as $filter_term ) { ?>
					<li>
						<a
								href="#" class="filter-term"
								data-filter_term="<?php echo esc_attr( $filter_term->slug ); ?>">
							<?php echo esc_html( $filter_term->name ); ?>
						</a>
					</li>
				<?php } ?>
			</ul>
			<div class="reset">
				<a href="#"><?php esc_html_e( 'Reset filter', 'typerion' ); ?></a>
			</div>
		<?php } ?>
	</div>
</div>
<div class="tab-content">
	<div class="tab-panel active" id="map-block">
		<div class="map-block">
			<div id="map"></div>
			<div class="items">
				<?php
				if ( $properties_query->have_posts() ) {

					while ( $properties_query->have_posts() ) {
						$properties_query->the_post();
						$property_id = get_the_ID();

						get_template_part( 'template_part/property', 'item', [ 'id' => $property_id ] );
					}
					wp_reset_postdata();
				}
				?>
			</div>
		</div>
	</div>
	<div class="tab-panel" id="grid">
		<div class="items">
			<?php
			if ( $properties_query->have_posts() ) {

				while ( $properties_query->have_posts() ) {
					$properties_query->the_post();
					$property_id = get_the_ID();

					get_template_part( 'template_part/property', 'item', [ 'id' => $property_id ] );
				}
				wp_reset_postdata();
			}
			?>
		</div>
	</div>
</div>
