<?php
/**
 * Add carbone fields in theme.
 *
 * @package typerion/theme
 */

namespace Tryperion\Theme;

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * CarboneField class file.
 */
class CarboneField {
	/**
	 * CarboneField construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	public function init(): void {
		add_action( 'after_setup_theme', [ $this, 'load_carbon_fields' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_area_data' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_theme_option' ] );
	}

	/**
	 * Load main class carbon fields.
	 *
	 * @return void
	 */
	public function load_carbon_fields(): void {
		Carbon_Fields::boot();
	}

	/**
	 * Add carbon field in area post type.
	 *
	 * @return void
	 */
	public function add_area_data(): void {

		Container::make( 'post_meta', __( 'Area data', 'typerion' ) )
		         ->where( 'post_type', '=', CPT::TR_CPT_AREA_NAME )
		         ->add_fields(
			         [
				         Field::make( 'text', 'tr_area_location', __( 'Area Location', 'typerion' ) ),
				         Field::make( 'text', 'tr_area_lat', __( 'Area latitude', 'typerion' ) )
				              ->set_width( 50 ),
				         Field::make( 'text', 'tr_area_lng', __( 'Area longitude', 'typerion' ) )
				              ->set_width( 50 ),
			         ]
		         );
	}

	/**
	 * Add theme options.
	 *
	 * @return void
	 */
	public function add_theme_option(): void {
		Container::make( 'theme_options', __( 'Theme Options', 'typerion' ) )
		         ->add_fields(
			         [
				         Field::make( 'text', 'tr_google_api_key', __( 'Google map api key' ) )
				              ->set_attribute( 'type', 'password' ),
			         ]
		         );
	}
}
