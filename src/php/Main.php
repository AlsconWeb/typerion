<?php
/**
 * Main theme class.
 *
 * @package typerion/theme
 */

namespace Tryperion\Theme;

use Elementor\Plugin;
use Tryperion\Theme\Elementor\Map;

/**
 * Main class file.
 */
class Main {

	public const TR_THEME_VERSION = '1.1.0';

	/**
	 * Main construct.
	 */
	public function __construct() {
		$this->init();

		new CPT();
		new CarboneField();
	}

	/**
	 * Add hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts' ] );
		add_action( 'init', [ $this, 'add_elementor_widgets' ] );
	}

	/**
	 * Add scripts and styles.
	 *
	 * @return void
	 */
	public function add_scripts(): void {
		$url = get_stylesheet_directory_uri();
		$min = '.min';

		if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
			$min = '';
		}

		wp_enqueue_script(
			'main',
			$url . '/assets/js/main' . $min . '.js',
			[
				'jquery',
				'google_map',
				'google_map_markers',
			],
			self::TR_THEME_VERSION,
			true
		);

		$google_map_key = carbon_get_theme_option( 'tr_google_api_key' );

		if ( ! empty( $google_map_key ) ) {
			wp_enqueue_script(
				'google_map',
				'https://maps.googleapis.com/maps/api/js?key=' . $google_map_key . '&v=weekly',
				[ 'jquery' ],
				'weekly',
				true
			);

			wp_enqueue_script(
				'google_map_markers',
				'//unpkg.com/@googlemaps/markerclusterer@2.0.2/dist/index.min.js',
				[
					'jquery',
					'google_map',
				],
				'weekly',
				true
			);
		}

		wp_enqueue_style( 'main', $url . '/assets/css/main' . $min . '.css', '', self::TR_THEME_VERSION );
	}

	/**
	 * Add elementor custom widget.
	 *
	 * @return void
	 */
	public function add_elementor_widgets(): void {
		Plugin::instance()->widgets_manager->register( new Map() );
	}
}
