<?php
/**
 * Add Custom post type.
 *
 * @package typerion/theme
 */

namespace Tryperion\Theme;

/**
 * CPT class file.
 */
class CPT {

	/**
	 * Custom post type properties by area name.
	 */
	public const TR_CPT_AREA_NAME = 'properties_by_area';

	/**
	 * Custom post type our team name.
	 */
	private const TR_CPT_OUR_TEAM_NAME = 'our_team';

	/**
	 * Custom taxonomy area category name.
	 */
	public const TR_CT_AREA_CATEGORY = 'area-category';

	/**
	 * Custom taxonomy area tag name.
	 */
	public const TR_CT_AREA_TAG = 'area-tag';

	/**
	 * CPT construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'init', [ $this, 'register_properties_by_area' ] );
		add_action( 'init', [ $this, 'register_our_team' ] );
		add_action( 'init', [ $this, 'register_area_type_tax' ] );
		add_action( 'init', [ $this, 'register_area_tag_tax' ] );
	}

	/**
	 * Register post type properties by area.
	 *
	 * @return void
	 */
	public function register_properties_by_area(): void {
		register_post_type(
			self::TR_CPT_AREA_NAME,
			[
				'label'         => null,
				'labels'        => [
					'name'               => __( 'Properties By Area', 'typerion' ),
					'singular_name'      => __( 'Properties By Area', 'typerion' ),
					'add_new'            => __( 'Add new area', 'typerion' ),
					'add_new_item'       => __( 'Add new area', 'typerion' ),
					'edit_item'          => __( 'Edit area', 'typerion' ),
					'new_item'           => __( 'New area', 'typerion' ),
					'view_item'          => __( 'View', 'typerion' ),
					'search_items'       => __( 'Search area', 'typerion' ),
					'not_found'          => __( 'Not found', 'typerion' ),
					'not_found_in_trash' => __( 'Not found in trash', 'typerion' ),
					'parent_item_colon'  => '',
					'menu_name'          => __( 'Properties By Area', 'typerion' ),
				],
				'description'   => '',
				'public'        => true,
				'show_in_menu'  => null,
				'show_in_rest'  => null,
				'rest_base'     => null,
				'menu_position' => 10,
				'menu_icon'     => 'dashicons-building',
				'hierarchical'  => false,
				'supports'      => [
					'title',
					'editor',
					'author',
					'thumbnail',
					'excerpt',
					'custom-fields',
					'revisions',
				],
				'taxonomies'    => [
					self::TR_CT_AREA_CATEGORY,
					self::TR_CT_AREA_TAG,
				],
				'has_archive'   => false,
				'rewrite'       => true,
				'query_var'     => true,
			]
		);
	}

	/**
	 * Add custom post type our team.
	 *
	 * @return void
	 */
	public function register_our_team(): void {
		register_post_type(
			self::TR_CPT_OUR_TEAM_NAME,
			[
				'label'         => null,
				'labels'        => [
					'name'               => __( 'Our Team', 'typerion' ),
					'singular_name'      => __( 'Our Team', 'typerion' ),
					'add_new'            => __( 'Add new team member', 'typerion' ),
					'add_new_item'       => __( 'Add new team member', 'typerion' ),
					'edit_item'          => __( 'Edit member', 'typerion' ),
					'new_item'           => __( 'New team member', 'typerion' ),
					'view_item'          => __( 'View member', 'typerion' ),
					'search_items'       => __( 'Search member', 'typerion' ),
					'not_found'          => __( 'Not found', 'typerion' ),
					'not_found_in_trash' => __( 'Not found in trash', 'typerion' ),
					'parent_item_colon'  => '',
					'menu_name'          => __( 'Our Team', 'typerion' ),
				],
				'description'   => '',
				'public'        => true,
				'show_in_menu'  => null,
				'show_in_rest'  => null,
				'rest_base'     => null,
				'menu_position' => 11,
				'menu_icon'     => 'dashicons-groups',
				'hierarchical'  => false,
				'supports'      => [
					'title',
					'editor',
					'author',
					'thumbnail',
					'excerpt',
					'custom-fields',
					'revisions',
				],
				'taxonomies'    => [],
				'has_archive'   => false,
				'rewrite'       => true,
				'query_var'     => true,
			]
		);
	}

	/**
	 * Register custom taxonomy category area.
	 *
	 * @return void
	 */
	public function register_area_type_tax(): void {
		register_taxonomy(
			self::TR_CT_AREA_CATEGORY,
			[ self::TR_CPT_AREA_NAME ],
			[
				'label'             => '',
				'labels'            => [
					'name'              => __( 'Area category', 'typerion' ),
					'singular_name'     => __( 'Area category', 'typerion' ),
					'search_items'      => __( 'Search area category', 'typerion' ),
					'all_items'         => __( 'All area category', 'typerion' ),
					'view_item '        => __( 'View area category', 'typerion' ),
					'parent_item'       => __( 'Parent area category', 'typerion' ),
					'parent_item_colon' => __( 'Parent category:', 'typerion' ),
					'edit_item'         => __( 'Edite category', 'typerion' ),
					'update_item'       => __( 'Update area category', 'typerion' ),
					'add_new_item'      => __( 'Add new category', 'typerion' ),
					'new_item_name'     => __( 'New area category', 'typerion' ),
					'menu_name'         => __( 'Area category', 'typerion' ),
					'back_to_items'     => __( '← Back to Category', 'typerion' ),
				],
				'description'       => '',
				'public'            => true,
				'hierarchical'      => true,
				'rewrite'           => true,
				'capabilities'      => [],
				'meta_box_cb'       => null,
				'show_admin_column' => false,
				'show_in_rest'      => null,
				'rest_base'         => null,
			]
		);
	}

	/**
	 * Register custom taxonomy tag area.
	 *
	 * @return void
	 */
	public function register_area_tag_tax(): void {
		register_taxonomy(
			self::TR_CT_AREA_TAG,
			[ self::TR_CPT_AREA_NAME ],
			[
				'label'             => '',
				'labels'            => [
					'name'              => __( 'Area tag', 'typerion' ),
					'singular_name'     => __( 'Area tag', 'typerion' ),
					'search_items'      => __( 'Search area tag', 'typerion' ),
					'all_items'         => __( 'All area tag', 'typerion' ),
					'view_item '        => __( 'View area tag', 'typerion' ),
					'parent_item'       => __( 'Parent area tag', 'typerion' ),
					'parent_item_colon' => __( 'Parent tag:', 'typerion' ),
					'edit_item'         => __( 'Edite tag', 'typerion' ),
					'update_item'       => __( 'Update area tag', 'typerion' ),
					'add_new_item'      => __( 'Add new tag', 'typerion' ),
					'new_item_name'     => __( 'New area tag', 'typerion' ),
					'menu_name'         => __( 'Area tag', 'typerion' ),
					'back_to_items'     => __( '← Back to Tags', 'typerion' ),
				],
				'description'       => '',
				'public'            => true,
				'hierarchical'      => false,
				'rewrite'           => true,
				'capabilities'      => [],
				'meta_box_cb'       => null,
				'show_admin_column' => false,
				'show_in_rest'      => null,
				'rest_base'         => null,
			]
		);
	}
}
