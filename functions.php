<?php
/**
 * Astra child theme.
 *
 * @package typerion/theme
 */

use Tryperion\Theme\Main;

/**
 * Add parent style.
 *
 * @return void
 */
function typerion_style(): void {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', [], Main::TR_THEME_VERSION );
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', [ 'parent-style' ], Main::TR_THEME_VERSION );
}

add_action( 'wp_enqueue_scripts', 'typerion_style' );

/**
 * Your code goes below.
 */

require_once 'vendor/autoload.php';

new Main();
