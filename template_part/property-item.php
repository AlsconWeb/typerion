<?php
/**
 * Property item template.
 *
 * @package typerion/theme
 */

use Tryperion\Theme\CPT;

$property_id = $args['id'];

$property_term = wp_get_post_terms( $property_id, CPT::TR_CT_AREA_CATEGORY );

$property_lat      = carbon_get_the_post_meta( 'tr_area_lat' );
$property_lng      = carbon_get_the_post_meta( 'tr_area_lng' );
$property_area     = carbon_get_the_post_meta( 'tr_area_location' );
$property_all_term = [];

foreach ( $property_term as $item ) {
	$property_all_term[] = $item->slug;
}
?>
<div
		class="item"
		data-area="<?php echo esc_attr( $property_area ); ?>"
		data-id="<?php echo esc_attr( $property_id ); ?>"
		data-lat="<?php echo esc_attr( $property_lat ); ?>"
		data-term="<?php echo esc_attr( implode( ',', $property_all_term ) ); ?>"
		data-lng="<?php echo esc_attr( $property_lng ); ?>">
	<a class="link" href="<?php the_permalink(); ?>" target="_blank"></a>
	<div class="img">
		<?php
		if ( has_post_thumbnail( $property_id ) ) {
			the_post_thumbnail( [ 200, 130 ] );
		} else {
			echo '<img src="https://placehold.co/200x130" alt="No image">';
		}
		?>
	</div>
	<div class="desc">
		<?php if ( ! empty( $property_term ) && ! is_wp_error( $property_term ) ) { ?>
			<p class="tag">
				<?php echo esc_html( $property_term[0]->name ?? '' ); ?>
			</p>
		<?php } ?>
		<h3><?php the_title(); ?></h3>
		<p><?php the_excerpt(); ?></p>
	</div>
</div>