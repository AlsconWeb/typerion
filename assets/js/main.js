/* global MarkerClusterer */

jQuery( document ).ready( function( $ ) {
	// tabs
	$( '[data-toggle="tab"]' ).click( function( event ) {
		event.preventDefault();

		let idEl = $( this ).attr( 'href' );

		$( '.nav-tabs li' ).removeClass( 'active' );
		$( '.tab-panel' ).removeClass( 'active' );
		$( this ).parent().addClass( 'active' );
		$( idEl ).addClass( 'active' );
	} );

	// maps
	let mapStyle = [
		{
			featureType: 'all',
			elementType: 'geometry.fill',
			stylers: [
				{
					weight: '2.00'
				}
			]
		}, {
			featureType: 'all',
			elementType: 'geometry.stroke',
			stylers: [
				{
					color: '#9c9c9c'
				}
			]
		}, {
			featureType: 'all',
			elementType: 'labels.text',
			stylers: [
				{
					visibility: 'on'
				}
			]
		}, {
			featureType: 'landscape',
			elementType: 'all',
			stylers: [
				{
					color: '#f2f2f2'
				}
			]
		}, {
			featureType: 'landscape',
			elementType: 'geometry.fill',
			stylers: [
				{
					color: '#ffffff'
				}
			]
		}, {
			featureType: 'landscape.man_made',
			elementType: 'geometry.fill',
			stylers: [
				{
					color: '#ffffff'
				}
			]
		}, {
			featureType: 'poi',
			elementType: 'all',
			stylers: [
				{
					visibility: 'off'
				}
			]
		}, {
			featureType: 'road',
			elementType: 'all',
			stylers: [
				{
					saturation: -100
				}, {
					lightness: 45
				}
			]
		}, {
			featureType: 'road',
			elementType: 'geometry.fill',
			stylers: [
				{
					color: '#eeeeee'
				}
			]
		}, {
			featureType: 'road',
			elementType: 'labels.text.fill',
			stylers: [
				{
					color: '#7b7b7b'
				}
			]
		}, {
			featureType: 'road',
			elementType: 'labels.text.stroke',
			stylers: [
				{
					color: '#ffffff'
				}
			]
		}, {
			featureType: 'road.highway',
			elementType: 'all',
			stylers: [
				{
					visibility: 'simplified'
				}
			]
		}, {
			featureType: 'road.arterial',
			elementType: 'labels.icon',
			stylers: [
				{
					visibility: 'off'
				}
			]
		}, {
			featureType: 'transit',
			elementType: 'all',
			stylers: [
				{
					visibility: 'off'
				}
			]
		}, {
			featureType: 'water',
			elementType: 'all',
			stylers: [
				{
					color: '#46bcec'
				}, {
					visibility: 'on'
				}
			]
		}, {
			featureType: 'water',
			elementType: 'geometry.fill',
			stylers: [
				{
					color: '#c8d7d4'
				}
			]
		}, {
			featureType: 'water',
			elementType: 'labels.text.fill',
			stylers: [
				{
					color: '#070707'
				}
			]
		}, {
			featureType: 'water',
			elementType: 'labels.text.stroke',
			stylers: [
				{
					color: '#ffffff'
				}
			]
		}
	];
	let markersArary = [];
	let markers = [];
	let map;
	let group;
	let allMarkers;
	let openInfoWindows = [];

	// map init
	async function initMap() {
		const { Map } = await google.maps.importLibrary( 'maps' );
		map = new Map( document.getElementById( 'map' ), {
			center: { lat: -34.397, lng: 150.644 },
			zoom: 10,
			disableDefaultUI: 0,
			backgroundColor: '#f4f4f4',
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			styles: mapStyle
		} );

		setMarkers();

		map.addListener( 'idle', function() {
			let bounds = map.getBounds();
			let visibleMarkers = [];

			for ( let i = 0; i < markers.length; i++ ) {
				markers[ i ].setMap( map );
				if ( bounds.contains( markers[ i ].getPosition() ) ) {
					visibleMarkers.push( markers[ i ] );
				}
			}

			hideProperties( visibleMarkers );
		} );
	}

	initMap();

	// set markers.
	function setMarkers() {
		let properties = $( '.map-block .items > .item' );

		properties.map( function( i, el ) {
			let markerItem = {
				lat: $( el ).data( 'lat' ),
				lng: $( el ).data( 'lng' )
			};

			markersArary.push( markerItem );

			markers[ i ] = new google.maps.Marker( {
				position: markerItem,
				map: map,
				title: $( el ).data( 'id' ).toString(),
			} );

			let infoWindow = new google.maps.InfoWindow( {
				content: $( el ).data( 'area' ).toString(),
				disableAutoPan: true,
			} );

			markers[ i ].addListener( 'click', function() {
				closeAllInfoWindows();
				infoWindow.open( map, markers[ i ] );
				openInfoWindows.push( infoWindow );

				let id = infoWindow.anchor.title;

				$( '[data-id=' + id + ']' ).addClass( 'active' );

				infoWindow.addListener( 'closeclick', function() {
					$( '[data-id=' + id + ']' ).removeClass( 'active' );
				} );
			} );


		} );

		group = new markerClusterer.MarkerClusterer( {
			map,
			markers
		} );

		allMarkers = markers;

		let bounds = new google.maps.LatLngBounds();
		let markersLength = markersArary.length;

		for ( let i = 0; i < markersLength; i++ ) {
			bounds.extend( markersArary[ i ] );
		}

		map.fitBounds( bounds );
	}

	// hide properties.
	function hideProperties( markers ) {
		let activeProperty = [];

		markers.forEach( function( el, i ) {
			activeProperty.push( Number( el.title ) );
		} );

		let elements = $( '#map-block .item[data-id]' );
		let lengthELs = elements.length;


		for ( let i = 0; i < lengthELs; i++ ) {
			let element = $( elements[ i ] );
			let id = $( element ).data( 'id' );

			if ( ! inArray( id, activeProperty ) ) {
				element.hide();
			} else if ( inArray( id, activeProperty ) && element.is( ':hidden' ) ) {
				element.show();
			}
		}
	}

	function closeAllInfoWindows() {
		for ( var i = 0; i < openInfoWindows.length; i++ ) {
			openInfoWindows[ i ].close();
			$( '.map-block .items .item.active' ).removeClass( 'active' );
		}
	}

	/**
	 * In array.
	 *
	 * @param value string|number
	 * @param array array
	 * @returns {boolean}
	 */
	function inArray( value, array ) {
		return array.indexOf( value ) !== -1;
	}

	// filter reset.

	let resetBtn = $( '.reset a' );

	if ( resetBtn.length ) {
		resetBtn.click( function( e ) {
			e.preventDefault();
			filterBtns.map( ( i, el ) => {
				$( el ).parent().removeClass( 'active' );
			} );

			setMarkers();
		} );
	}

	if ( $( window ).width() < 1200 ) {
		let resetButton = $( '.reset' );
		$( '.filter' ).after( resetButton );
		$( '.filter p' ).click( function() {
			$( this ).next().slideToggle();
		} );
	}

	let filterBtns = $( '.filter-term' );

	if ( filterBtns.length ) {
		filterBtns.click( function( e ) {
			e.preventDefault();

			filterBtns.map( function( i, el ) {
				$( el ).parent().removeClass( 'active' );
			} );

			$( this ).parent().addClass( 'active' );

			let markersA = getFilterMarkers( $( this ).data( 'filter_term' ) );

			hideProperties( markersA );

		} );
	}

	/**
	 * Filter get new markers.
	 *
	 * @param  term string.
	 * @returns {*[]}
	 */
	function getFilterMarkers( term ) {
		let properties = $( '.map-block .item' );
		let markersA = [];
		let iterator = 0;
		let merkersFromBouns = [];

		properties.map( function( i, el ) {
			let propertyTerm = $( el ).data( 'term' ).split( ',' );
			if ( inArray( term, propertyTerm ) ) {
				let markerItem = {
					lat: $( el ).data( 'lat' ),
					lng: $( el ).data( 'lng' )
				};

				markersA.push( markerItem );
				merkersFromBouns.push( markerItem );

				markersA[ iterator ] = new google.maps.Marker( {
					position: markerItem,
					map: map,
					title: $( el ).data( 'id' ).toString(),
				} );

				let infoWindow = new google.maps.InfoWindow( {
					content: $( el ).data( 'area' ).toString(),
					disableAutoPan: true,
				} );


				markersA[ iterator ].addListener( 'click', function() {
					closeAllInfoWindows();

					infoWindow.open( {
						anchor: this,
						map,
					} );

					// console.log( infoWindow );
					openInfoWindows.push( infoWindow );

					let id = infoWindow.anchor.title;

					$( '[data-id=' + id + ']' ).addClass( 'active' );

					infoWindow.addListener( 'closeclick', function() {
						$( '[data-id=' + id + ']' ).removeClass( 'active' );
					} );
				} );

				iterator++;
			}
		} );


		for ( let i = 0; i < markers.length; i++ ) {
			markers[ i ].setMap( null );
		}

		markers = markersA;

		for ( let i = 0; i < markers.length; i++ ) {
			markers[ i ].setMap( map );
		}

		if ( group ) {
			group.clearMarkers();
		}

		let bounds = new google.maps.LatLngBounds();
		let markersLength = merkersFromBouns.length;

		for ( let i = 0; i < markersLength; i++ ) {
			bounds.extend( merkersFromBouns[ i ] );
		}

		map.fitBounds( bounds );

		group = new markerClusterer.MarkerClusterer( map, markers );

		return markersA;
	}

} );
